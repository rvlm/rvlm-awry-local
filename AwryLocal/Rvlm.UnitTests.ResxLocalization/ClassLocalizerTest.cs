﻿using System;
using System.Globalization;
using System.Threading;
using Rvlm.ResxLocalization;
using NUnit.Framework;

namespace Rvlm.UnitTests.ResxLocalization
{
    [TestFixture]
    public class ClassLocalizerTest
    {
        static ClassLocalizer localizer = new ClassLocalizer(typeof(ClassLocalizerTest));
        
        [Test]
        [SetUICulture("")]
        public void NeutralCulture()
        {
            Assert.AreEqual("neutral", localizer.Tr("culture"));
        }

        [Test]
        [SetUICulture("ja")]
        public void JapaneseCulture()
        {
            Assert.AreEqual("ja", localizer.Tr("culture"));
        }

        [Test]
        [SetUICulture("es")]
        public void SpanishCulture()
        {
            Assert.AreEqual("es", localizer.Tr("culture"));
        }

        [Test]
        [SetUICulture("es-AR")]
        public void ArgentineanSpanishCulture()
        {
            Assert.AreEqual("es-AR", localizer.Tr("culture"));
        }

        [Test]
        [SetUICulture("es-MX")]
        public void MexicanSpanishCulture()
        {
            // Так как отдельного варианта для мексиканского диалекта в тесте
            // не предусмотрено, будет возвращен вариант для испанского.
            Assert.AreEqual("es", localizer.Tr("culture"));
        }

        [Test]
        [SetUICulture("th")]
        public void ThaiCulture()
        {
            // Так как варианта для тайского языка нет, будет возращен текст
            // из нейтральной культуры.
            Assert.AreEqual("neutral", localizer.Tr("culture"));
        }
    }
}
