﻿using System;
using System.Collections.Generic;
using System.IO;
using AWcore;
using AWcore.Po;
using AWcore.ResX;

namespace AWupdate
{
    static class Program
    {
        /**
         * Program entry point.
         * 
         * Gathers translatable strings from PO file to RESX files.
         * Invokation syntax is as follows:
         * @code
         *     AWlookup POFILE DIRECTORY
         * @endcode
         * where @c POFILE is the file to store translatable strings (it may exist or
         * not) and @c DIRECTORY is the project or solution directory, in which all
         * RESX files for invariant culture will be scanned for translatable strings.
         */
        [STAThread]
        static void Main(string[] argv)
        {
            if (argv.Length < 2)
            {
                Console.Error.WriteLine("Not enough parameters given.");
                Environment.Exit(1);
            }

            string outputFile = argv[0];
            string baseDirectory = argv[1];
            string targetLanguage = argv.Length > 2 ? argv[2] : null;

            MessageFile messageFile = new MessageFile();

            ResourceFilesPool pool = new ResourceFilesPool();

            DirectoryInfo baseDir = new DirectoryInfo(baseDirectory);
            foreach (FileInfo info in baseDir.GetFiles("*.resx", SearchOption.AllDirectories))
            {
                if (FileNamesHelper.GetMiddleExtension(info.Name) != "")
                    continue;

                // We will need relative name later for writing
                // message entry reference.
                string relativeName =
                    FileNamesHelper.GetRelativePath(info.FullName, baseDir.FullName);

                Console.WriteLine(" -- processing {0}", relativeName);

                ResourceFile inputResx = pool.GetResourceFile(info.FullName, true);
                foreach (var pair in inputResx.Strings)
                {
                    if (string.IsNullOrEmpty(pair.Key))
                        continue;

                    // We must skip all entries starting from special characters,
                    // as they may have special meaning in Windows Forms. The only
                    // exception is "$" what starts "$this" resource name.
                    char first = pair.Key[0];
                    if (Char.IsLetter(first) || first == '$')
                    {
                        MessageEntry entry = new MessageEntry();
                        entry.Original = pair.Value;
                        entry.References.Add(relativeName);

                        string translation = null;
                        if (!string.IsNullOrEmpty(targetLanguage))
                        {
                            ResourceFile localized = pool.GetResourceFile(
                                FileNamesHelper.ChangeMiddleExtension(
                                    inputResx.FileName,
                                    targetLanguage), true);

                            translation = localized.GetString(pair.Key);
                        }

                        entry.Translation = translation;

                        messageFile.Add(entry);
                    }
                }
            }

            using (StreamWriter writer = new StreamWriter(outputFile, true))
            {
                messageFile.WriteTo(writer);
            }

            pool.Close();
        }
    }
}
