﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using AWcore;
using AWcore.Po;
using AWcore.ResX;

namespace AWrelease
{
    static class Program
    {
        /**
         * Program entry point.
         * 
         * Scatters back translated strings from PO file to RESX files.
         * Invokation syntax is as follows:
         * @code
         *     AWrelease POFILE DIRECTORY
         * @endcode
         * where @c POFILE is the file with translated messages ready for embedding
         * into the project, @c DIRECTORY is the project or solution directory, which
         * was previosly given to @c AWlookup program. Target language is determined
         * automatically by the name of @c POFILE, which must have middle extension
         * specifying target language, e.g. @c messages.ru_RU.po.
         */
        [STAThread]
        static void Main(string[] argv)
        {
            if (argv.Length < 2)
            {
                Console.Error.WriteLine("Not enough parameters given.");
                Environment.Exit(1);
            }

            string inputFile = argv[0];
            string baseDirectory = argv[1];
            string targetLanguage = FileNamesHelper.GetMiddleExtension(inputFile);

            // Check if there really exists such a language.
            CultureInfo targetCulture = null;

            try
            {
                targetCulture = CultureInfo.GetCultureInfo(targetLanguage);
            }
            catch
            {
                Console.Error.WriteLine("Wrong culture name.");
                Environment.Exit(1);
            }

            if (targetCulture == null || targetCulture.Name == "")
            {
                Console.Error.WriteLine("Unable to determine target language.");
                Environment.Exit(1);
            }

            // If language exists, get its "canonical" identifier. User supplied
            // string may, for example, has wrong character case.
            targetLanguage = targetCulture.Name;

            // Read input file *before* changing current directory.
            MessageFile messageFile = MessageFile.ReadFromFile(inputFile);
            if (messageFile == null)
            {
                Console.Error.WriteLine("Unable to read input file.");
                Environment.Exit(1);
            }

            // Change current directory as all references in message file are relative
            // to its base directory.
            try
            {
                Environment.CurrentDirectory = baseDirectory;
            }
            catch
            {
                Console.Error.WriteLine("Unable to change current directory.");
                Environment.Exit(1);
            }

            ResourceFilesPool pool = new ResourceFilesPool();

            foreach (MessageEntry entry in messageFile)
            {
                foreach (string reference in entry.References)
                {
                    string[] components = reference.Split(":".ToCharArray(), 2);
                    string fileName = components[0];
                    string lineNum  = (components.Length > 1) ? components[1] : "";

                    string extension = Path.GetExtension(fileName);

                    List<string> keys = new List<string>();
                    if (extension.ToLower() != "resx")
                    {
                        keys.Add(entry.Original);
                    }
                    else
                    {
                        // Open resx file with untranslated strings and lookup
                        // for appropriate key.
                        ResourceFile resx = pool.GetResourceFile(fileName, true);
                        foreach (KeyValuePair<string,string> pair in resx.Strings)
                        {
                            if (pair.Value == entry.Original)
                                keys.Add(pair.Key);
                        }
                    }                    

                    if (keys.Count == 0)
                    {
                        Console.Error.WriteLine("Unable to find string key.");
                        continue;
                    }

                    string resxFileName = 
                        FileNamesHelper.ChangeMiddleExtension(
                            Path.ChangeExtension(fileName, "resx"), targetLanguage);

                    ResourceFile targetResx = pool.GetResourceFile(resxFileName);
                    foreach (string key in keys)
                    {
                        targetResx.SetString(key, entry.Translation);
                    }
                } // foreach (reference)
            } // foreach (entry)

            pool.Close();
        }
    }
}
