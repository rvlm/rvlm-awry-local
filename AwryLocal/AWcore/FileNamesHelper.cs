﻿using System;
using System.IO;
using System.Text;

namespace AWcore
{
    /** Helper class for dealing with file paths. */
    public static class FileNamesHelper
    {
        /**
         * Extracts "middle" extension from given @a fileName.
         * For example, if file name is @c "Resources.ru_RU.resx", then its middle
         * extension is @c "ru-RU". Visual Studio uses such extensions to link files
         * for different cultures.
         * 
         * @note
         * Unlike @c Path.GetExtension this method @em doesn't prepend @c "." in front
         * of extension.
         * 
         * @see ChangeMiddleExtension
         */
        public static string GetMiddleExtension(string fileName)
        {
            string extensionWithDot = Path.GetExtension(
                Path.GetFileNameWithoutExtension(fileName));

            return extensionWithDot.TrimStart('.');
        }

        /**
         * Changes "middle" extension for given @a fileName to @a middleExt.
         * If file name doesn't have middle extension then it will be inserted.
         * @see GetMiddleExtension
         */
        public static string ChangeMiddleExtension(string fileName, string middleExt)
        {
            string baseName = Path.GetFileNameWithoutExtension(fileName);
            string extension = Path.GetExtension(fileName);

            return Path.Combine(
                Path.GetDirectoryName(fileName),
                Path.ChangeExtension(baseName, middleExt) + extension);
        }

        /**
         * Gets relative file path based on given directory.
         * 
         * Method return path for given @a fileName from directory @a directoryName.
         * For example:
         * @code
         *     GetRelativePath(@"c:\Windows\System32\Drivers\hosts", @"c:\Windows");
         *     // will return @"System32\Drivers\hosts"
         *     
         *     GetRelativePath(@"c:\Users\A\Local Settings", @"c:\Users\B\Documents");
         *     // will return @"..\..\A\Local Settings"
         * @endcode
         * 
         * Note that this method does @em not check paths for existence. Everything is
         * done on the level of operations with strings.
         */
        public static string GetRelativePath(string fileName, string directoryName)
        {
            char[] delimiters = new char[] {
                Path.DirectorySeparatorChar,
                Path.AltDirectorySeparatorChar,
            };

            string[] fileComponents = 
                fileName.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            string[] directoryComponents =
                directoryName.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            int length = Math.Min(
                fileComponents.Length,
                directoryComponents.Length);

            // TODO: Sometimes comparing must be case-insensivite.
            int idx = 0;
            while (idx<length && directoryComponents[idx] == fileComponents[idx])
            {
                idx++;
            }

            StringBuilder sb = new StringBuilder();

            for (int i=idx; i<directoryComponents.Length; i++)
            {
                sb.Append("..");
                sb.Append(Path.DirectorySeparatorChar);
            }

            for (int i=idx; i<fileComponents.Length; i++)
            {
                sb.Append(fileComponents[i]);
                sb.Append(Path.DirectorySeparatorChar);
            }

            if (sb.Length>0)
                sb.Remove(sb.Length-1, 1);

            return sb.ToString();
        }
    }
}
