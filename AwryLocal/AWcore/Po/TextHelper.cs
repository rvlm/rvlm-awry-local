﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AWcore.Po
{
    public static class TextHelper
    {
        private static readonly int Width = 76;
        private static readonly char Quote = '\"';
        private static readonly char Escape = '\\';

        private enum ParserState
        {
            Normal,
            MeetBackslash,
        }

        public static string ReadQuoted(this TextReader reader)
        {
            throw new NotImplementedException();

            int first = reader.Peek();
            if (first < 0)
                return null;

            if ((char)first != Quote)
                throw new FormatException();

            reader.Read();

            // \\ \" \000 \x00 \u00000
            StringBuilder result = new StringBuilder();
            while (true)
            {
                int current = reader.Peek();
                if (current < 0)
                    throw new FormatException();

                char ch = (char)current;
            }            
        }

        public static string ReadQuotedLine(this TextReader reader)
        {
            throw new NotImplementedException();
        }

        public static void WriteQuoted(this TextWriter writer, string text)
        {
            if (text == null)
                text = "";

            writer.Write('\"');

            if (text.IndexOf('\n') >= 0)
            {
                writer.Write('\"');
                writer.WriteLine();
                writer.Write('\"');
            }

            int currentWidth = 0;
            foreach (char ch in text)
            {
                if (currentWidth >= Width)
                {
                    writer.Write('\"');
                    writer.WriteLine();
                    writer.Write('\"');
                    currentWidth = 0;
                }

                switch (ch)
                {
                    case '\'': writer.Write("\\\'"); break;
                    case '\"': writer.Write("\\\""); break;
                    case '\\': writer.Write("\\\\"); break;
                    case '\0': writer.Write("\\0"); break;
                    case '\a': writer.Write("\\a"); break;
                    case '\b': writer.Write("\\b"); break;
                    case '\f': writer.Write("\\f"); break;
                    case '\n': writer.Write("\\n"); break;
                    case '\r': writer.Write("\\r"); break;
                    case '\t': writer.Write("\\t"); break;
                    case '\v': writer.Write("\\v"); break;

                    default:
                        writer.Write(ch);
                        break;
                }
                
                currentWidth++;
            }

            writer.Write(Quote);
        }

        public static void WriteQuotedLine(this TextWriter writer, string text)
        {
            writer.WriteQuoted(text);
            writer.WriteLine();
        }

        public static void WriteMultilineComment(
            this TextWriter writer, string marker, IEnumerable<string> comment)
        {
            throw new NotImplementedException();
        }
    }
}
