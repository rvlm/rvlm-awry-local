﻿using System;
using System.IO;
using System.Collections.Generic;

namespace AWcore.Po
{
    /**
     * Translation message entry.
     * 
     * This class represents single entry in PO file with all identifiers, strings and
     * comments. It can be read from file and written back
     */
    public class MessageEntry
    {
        /** Creates instance of class. */
        public MessageEntry()
        {
            References = new List<string>();
            Flags      = new List<string>();
            Comments   = new List<string>();
        }

        /**
         * 
         */
        public string Context { get; set; }

        /**
         * 
         */
        public string Original { get; set; }

        /**
         * 
         */
        public string Translation { get; set; }

        /**
         * 
         */
        public List<string> References { get; private set; }

        /**
         * 
         */
        public List<string> Flags { get; private set; }

        /**
         * 
         */
        public List<string> Comments { get; private set; }

        /**
         * 
         */
        public void WriteTo(TextWriter streamWriter)
        {
            // ... write comments.

            foreach (string reference in References)
                streamWriter.WriteLine("#: {0}", reference);

            if (Context != null)
            {
                streamWriter.Write("msgctxt ");
                streamWriter.WriteQuotedLine(Context);
            }

            streamWriter.Write("msgid ");
            streamWriter.WriteQuotedLine(Original);

            streamWriter.Write("msgstr ");
            streamWriter.WriteQuotedLine(Translation);

            streamWriter.WriteLine();
        }

        /**
         * 
         */
        public static MessageEntry ReadFrom(TextReader streamReader)
        {
            throw new NotImplementedException();
        }
    }
}
