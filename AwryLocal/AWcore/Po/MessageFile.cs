﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AWcore.Po
{
    public class MessageFile : List<MessageEntry>
    {
        public Encoding CatalogEncoding { get; set; }

        public string GetHeaderValue(string header)
        {
            throw new NotImplementedException();
        }

        public void SetHeaderValue(string header, string value)
        {
            throw new NotImplementedException();
        }

        public void WriteTo(TextWriter streamWriter)
        {
            Sort((a, b) => string.Compare(a.Original, b.Original));
            foreach (MessageEntry entry in this)
            {
                entry.WriteTo(streamWriter);
            }
        }

        public static MessageFile ReadFromFile(string fileName)
        {
            throw new NotImplementedException();
        }

        public static Encoding DetectFileEncoding(TextReader streamReader)
        {
            throw new NotImplementedException();
        }
    }
}
