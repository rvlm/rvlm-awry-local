﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace AWcore.ResX
{
    /**
     * Convenience class for working with *.resx resource files.
     * 
     * This class separates string resources (acessible via @c Strings property) from
     * other resources in file (@c Objects property). It reads existing resources after
     * creation, allows user to add or replace some strings, and writes changes back
     * on closing.
     * 
     * For example, the following code will write some string data to file @c temp.resx.
     * If this file doesn't exist, it will be created, if not, all existing data will be
     * kept. (Note: except comments!)
     * @code
     *     ResourceFile res = new ResourceFile("temp.resx");
     *     res.SetString("Universal answer", "42");
     *     res.SetString("Corporation of Evil", "Microsoft");
     *     res.Close();
     * @endcode
     * 
     * Better approach with @c using statement is also supported. File will be closed
     * automatically when leaving @c using block:
     * @code
     *     using (ResourceFile res = new ResourceFile("temp.resx"))
     *     {
     *         // ... operations
     *     }
     * @endcode
     * 
     * Resource file may be opened read-only. In that case any content modifications
     * will not be written back on close. Optional boolean argument must be passed
     * to constructor in order to open file read-only:
     * @code
     *     ResourceFile res = new ResourceFile("temp.resx", true);
     * @endcode
     */
    public class ResourceFile
    {
        /**
         * Created class instance and opens resource file.
         * If given @a fileName exists, its contents gets loaded into memory buffer.
         * File is opened read-write.
         * @see FileName
         */
        public ResourceFile(string fileName)
            : this(fileName, false) { }

        /**
         * Created class instance and opens resource file.
         * If given @a fileName exists, its contents gets loaded into memory buffer.
         * File is opened read-only if @a readOnly is set to @c true.
         * @see FileName
         * @see ReadOnly
         */
        public ResourceFile(string fileName, bool readOnly)
        {
            XmlDom = new XmlDocument();
            Strings = new Dictionary<string,string>();

            ReadOnly = readOnly;
            FileName = fileName;

            if (File.Exists(fileName))
                Load(fileName);
        }

        /** File name which was given to constructor. */
        public string FileName { get; private set; }

        /** Don't write changes back. */
        public bool ReadOnly { get; private set; }

        /**
         * Strings stored in resource file.
         * Any changes in this collection will be applied on close.
         */
        public Dictionary<string,string> Strings { get; private set; }

        /**
         * Non-string objects stored in resource file.
         * Any changes in this collection will be applied on close.
         */
        public XmlDocument XmlDom { get; private set; }

        /** Gets resource string with given @a key. */
        public string GetString(string key)
        {
            string result;
            Strings.TryGetValue(key, out result);

            return result;
        }

        /** Sets new @a value to resource string with given @a key. */
        public void SetString(string key, string value)
        {
            Strings[key] = value;
        }

        /**
         * Closes file.
         * If the object was not created in read-only mode then also writes changes back.
         * This method is automatically called by @c Dispose.
         * @see ReadOnly
         * @see Dispose
         */
        public void Close()
        {
            if (!ReadOnly)
                Save(FileName);

            Strings.Clear();
        }

        /**
         * Disposes resource file.
         * Do the same thing as @c Close. This method must be defined in order
         * to support @c using syntactic sugar.
         * @see Close
         */
        public void Dispose()
        {
            Close();
        }

        /**
         * @internal
         * Loads all data from given @a fileName to memory.
         */
        private void Load(string fileName)
        {
            XmlDom.Load(fileName);
            Strings.Clear();

            foreach (XmlNode node in XmlDom.SelectNodes("/root/data[@name]"))
            {
                if (node.Attributes["type"] != null)
                    continue;

                string key = node.Attributes["name"].Value;
                string val = node["value"].InnerText;

                Strings.Add(key, val);
                node.ParentNode.RemoveChild(node);
            }
        }

        /**
         * @internal
         * Writes all changes from memory back to @a fileName.
         */
        private void Save(string fileName)
        {
            XmlNode rootNode = XmlDom.SelectSingleNode("/root");
            if (rootNode == null)
                XmlDom.AppendChild(XmlDom.CreateElement("root"));

            foreach (var pair in Strings)
            {
                XmlAttribute attibute = XmlDom.CreateAttribute("name");
                attibute.Value = pair.Key;

                XmlElement element = XmlDom.CreateElement("data");
                element.Value = pair.Value;

                element.Attributes.Append(attibute);
                rootNode.AppendChild(element);
            }

            XmlDom.Save(fileName);
        }
    }
}
