﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AWcore.ResX
{
    /**
     * Pool of @c ResourceFile objects.
     * 
     * Pool may be asked for @c ResourceFile object for particular file name. If there
     * is no cached object, the new one created.
     * 
     * For example, the following code will perform writing string values to all *.resx
     * files in the current directory.
     * @code
     *     ResourceFilesPool pool = new ResourceFilesPool();
     *     DirectoryInfo currentDir = new DirectoryInfo(".");
     *     foreach (FileInfo info in currentDir.GetFiles("*.resx"))
     *     {
     *         pool.GetResourceFile(info.FullName).SetStringValue("ID", "42");
     *     }
     *     pool.Close();
     * @endcode
     * 
     * And, of course, @c using syntactic sugar is also supported. 
     */
    public class ResourceFilesPool : IDisposable
    {
        private Dictionary<string,ResourceFile> poolObjects;

        /** Creates new instance of class. */
        public ResourceFilesPool()
        {
            poolObjects = new Dictionary<string,ResourceFile>();
        }

        /**
         * Returns new or cached @c ResourceFile object for given @a fileName.
         * If new object must be created, it will be created with read-write mode.
         */
        public ResourceFile GetResourceFile(string fileName)
        {
            return GetResourceFile(fileName, false);
        }

        /**
         * Returns new or cached @c ResourceFile object for given @a fileName.
         * If new object must be created, @a readOnly parameter will be taken into
         * account.
         */
        public ResourceFile GetResourceFile(string fileName, bool readOnly)
        {
            FileInfo info = new FileInfo(fileName);

            ResourceFile result;
            bool found = poolObjects.TryGetValue(info.FullName, out result);
            
            if (!found)
            {
                result = new ResourceFile(fileName, readOnly);
                poolObjects.Add(info.FullName, result);
            }
            
            return result;
        }

        /** Closes all files in the pool. */
        public void Close()
        {
            foreach (ResourceFile resFile in poolObjects.Values)
            {
                resFile.Close();
            }

            poolObjects.Clear();
        }

        /**
         * Disposes resource files pool.
         * Do the same thing as @c Close. This method must be defined in order
         * to support @c using syntactic sugar.
         * @see Close
         */
        public void Dispose()
        {
            Close();
        }
    }
}
