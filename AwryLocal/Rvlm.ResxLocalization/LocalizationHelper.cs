﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace Rvlm.ResxLocalization
{
    public class LocalizationHelper
    {
        public static string GetLocalizedString(IDictionary<string,string> dictionary)
        {
            return GetLocalizedString(dictionary, CultureInfo.CurrentUICulture);
        }

        public static string GetLocalizedString(
            IDictionary<string,string> dictionary, CultureInfo cultureInfo)
        {
            Debug.Assert(dictionary != null);
            Debug.Assert(cultureInfo != null);
            
            CultureInfo culture = cultureInfo;
            string localizedVal = null;

            do
            {
                if (dictionary.TryGetValue(culture.Name, out localizedVal))
                    break;

                culture = culture.Parent;
            }
            while (culture != CultureInfo.InvariantCulture);

            return localizedVal;
        }
    }
}
