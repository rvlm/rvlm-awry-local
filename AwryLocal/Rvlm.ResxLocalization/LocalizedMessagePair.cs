﻿using System;

namespace Rvlm.ResxLocalization
{
    public sealed class LocalizedMessagePair
    {
        public LocalizedMessagePair(string neutralMsg, string localizedMsg)
        {
            NeutralMessage   = neutralMsg;
            LocalizedMessage = localizedMsg;
        }

        public string NeutralMessage { get; private set; }

        public string LocalizedMessage { get; private set; }
    }
}
