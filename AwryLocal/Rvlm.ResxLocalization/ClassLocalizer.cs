﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Rvlm.ResxLocalization
{
    /**
     * Class-based gettext-style localization helper.
     * Provides various @c Tr* methods for easy message translation into end-user culture
     * language. The usage example:
     * @code
     *     public class MainForm : Form 
     *     {
     *         private static readonly ClassLocalizer localizer =
     *             new ClassLocalizer(typeof(MainForm));
     *
     *         public MainForm()
     *         {
     *             exitButton.Text = localizer.Tr("Exit");
     *         }
     *     }
     * @endcode
     */
    public sealed class ClassLocalizer
    {
        private ComponentResourceManager resourceManager;

        /**
         * Constructs class object for translating @a componentType.
         */
        public ClassLocalizer(Type componentType)
        {
            resourceManager = new ComponentResourceManager(componentType);
        }

        /**
         * Translates @a message into current user interface culture.
         * This method returns translated string or, if no appropriate translation found,
         * original untranslated @a message. This method relies on standard string
         * resources system of .NET framework.
         * @see TrPair
         */
        public string Tr(string message)
        {
            string result = resourceManager.GetString("@@" + message);
            return (result != null) ? result : message;
        }

        /**
         * Translates @a message into current user interface culture.
         * Unlike @c Tr method, this method returns a pair of strings: the original
         * message as key and the translated message as value.
         * @see Tr
         */
        public LocalizedMessagePair TrPair(string message)
        {
            return new LocalizedMessagePair(message, Tr(message));
        }
    }
}
